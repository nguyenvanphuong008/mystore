<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import="myproject.springmvc.utils.SecurityUtils" %>
 <%@include file="/common/taglib.jsp" %>
<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone "></i> +021-95-51-84</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> 1734 Stonecoal Road</a></li>
					</ul>
					<ul class="header-links pull-right">
						<li><a href="#"><i class="fa fa-dollar"></i> USD</a></li>
						<sec:authorize access="isAnonymous()">
						<li><a href="${pageContext.request.contextPath}/login"><i class="fa fa-user-o"></i>Login</a></li>
						<li><a href="#"><i class="fa fa-user-o"></i>Register</a></li>
						</sec:authorize>
						<sec:authorize access="isAuthenticated()">
						<li><a href="#">Welcome: <%=SecurityUtils.getPrincipal().getFullName() %> </a></li>
						<li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-user-o"></i> Logout</a></li>
						</sec:authorize>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img src="${pageContext.request.contextPath}/template/admin/img/logo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->
						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									<select class="input-select">
										<option value="0">All Categories</option>
										<option value="1">Category 01</option>
										<option value="1">Category 02</option>
									</select>
									<input class="input" placeholder="Search here">
									<button class="search-btn">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->
						
						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									<a href="#">
										<i class="fa fa-heart-o"></i>
										<span>Your Wishlist</span>
										<div class="qty">2</div>
									</a>
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="dropdown" >
										<a href="${pageContext.request.contextPath}/view-cart"  >
											<i  class="fa fa-shopping-cart"></i>
										<span>Your Cart</span>
										<div  class="qty" id="qty">${totalItem}
										</div>
									</a>
									<%-- <c:if test="${not empty order }">
											<a href="${pageContext.request.contextPath}/view-cart"  >
												<i class="fa fa-shopping-cart"></i> <span>Your Cart</span>
												<div class="qty" id="qty" id="addCart">
													5
												</div>
											</a>
										</c:if> --%>
								</div>
								<!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->