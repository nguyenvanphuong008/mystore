<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
	<li class="breadcrumb-item active">Blank Page</li>
</ol>
<div class="card mb-3">
	<div class="card-header">
		<div class="row">
			<div >
			<a href="${pageContext.request.contextPath}/admin/add-product.html" style="text-decoration: none;">
				<img alt="" src="${pageContext.request.contextPath}/template/admin/img/create.png"> <strong>Add new</strong>
			</a>
		</div>
		<div style="margin:auto;">
			<h5><strong>Data Table Product</strong></h5>
		</div>
		<form:form action="${pageContext.request.contextPath}/admin/search-product.html"
			class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
			<div class="input-group">
				<input name="keyWord" type="text" class="form-control" placeholder="Search for..."
					aria-label="Search" aria-describedby="basic-addon2"/>
				<div class="input-group-append">
					<button class="btn btn-primary" type="submit">
						<i class="fas fa-search"></i>
					</button>
				</div>
			</div>
		</form:form>
		</div>
	</div>
	
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align: center;">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Price</th>
						<th>Sale off</th>
						<th>Quantity</th>
						<th>Category</th>
						<th>Brand</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Price</th>
						<th>Sale off</th>
						<th>Quantity</th>
						<th>Category</th>
						<th>Brand</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="o" items="${list }">
						<tr>
							<td >${o.id }</td>
							<td >${o.name }</td>
							<td >${o.price }</td>
							<td >${o.saleOff }</td>
							<td>${o.quantity }</td>
							<td >${o.category.name }</td>
							<td >${o.brand.name }</td>
							<td><a href="${pageContext.request.contextPath}/admin/edit-product.html?id=${o.id}"><img src="${pageContext.request.contextPath}/template/admin/img/edit.png"></a></td>
							<td><a href=""><img src="${pageContext.request.contextPath}/template/admin/img/trash.png"></a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
