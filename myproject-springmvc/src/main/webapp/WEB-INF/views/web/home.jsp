<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<c:url var="addItemURL" value="/add-item"/>
			<!-- /SECTION MAIN  -->
			<%@ include file="/common/web/main-section.jsp" %>
		<!-- /SECTION MAIN -->
		
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">New Products</h3>
							<div class="section-nav">
								<ul class="section-tab-nav tab-nav">
									<li class="active"><a data-toggle="tab" href="#tab1">Laptops</a></li>
									<li><a data-toggle="tab" href="#tab1">Smartphones</a></li>
									<li><a data-toggle="tab" href="#tab1">Cameras</a></li>
									<li><a data-toggle="tab" href="#tab1">Accessories</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										<!-- product -->
										<c:forEach var="o" items="${listOfProduct}">
											<div class="product">
											<div class="product-img">
												<img src="${pageContext.request.contextPath}/upload/${o.image}" alt="">
												<div class="product-label">
													<span class="sale">-30%</span>
													<span class="new">NEW</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">${o.category.name }</p>
												<h3 class="product-name"><a href="#">${o.name }</a></h3>
												<h4 class="product-price">$${o.price } <del class="product-old-price">$990.00</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
												<div class="add-to-cart">
												<input type="hidden" value="${o.id}" id="addItem_${o.id }">
												<button class="add-to-cart-btn"  data-id ="${o.id }"><i class="fa fa-shopping-cart"  ></i> add to cart</button>
											</div>
										</div>
										</c:forEach>
										<!-- /product -->
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- HOT DEAL SECTION -->
			<%@ include file="/common/web/hot-deal.jsp" %>
		<!-- /HOT DEAL SECTION -->

		<!-- /SECTION -->
		<!-- /SECTION -->
		<script>
			var clickAddCart= $('.add-to-cart-btn');
			console.log(clickAddCart.text());
			
			clickAddCart.each(function() {
				$(this).click(function() {
					var data = $(this).data('id');
					$.ajax({
			            url: '${pageContext.request.contextPath}/add-item',
			            type: 'POST',
			            data: {data:data, quantity: 1},
			            success: function (result) {
			            	alert(result)
			            	var qty = result;
			            	$('#qty').html(qty);
			            },
			            error: function (error) {
			            	alert('loi roi');
			            }
			        });
				})
			});
			
			/* function addItem() {
				$.ajax({
					url: '${addItemURL}',
		            type: 'post',
		           	contentType: 'application/json',
		            data: JSON.stringify(data),
		            dataType: 'json', 
		            success: function (result) {
		            },
		            error: function (error) {
		            }
				}); 
			}  */
		</script>

		