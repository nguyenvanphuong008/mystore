<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Your cart</h3>
				<ul class="breadcrumb-tree">
					<li><a href="#">Home</a></li>
					<li class="active">Your cart</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->
<c:if test="${not empty order }">
<div class="container">
	<!-- row -->
	<div class="row">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="text-align: center;">IMAGE</th>
					<th style="text-align: center;">PRODUCT</th>
					<th style="text-align: center;">PRICE</th>
					<th style="text-align: center;">QUANTITY</th>
					<th style="text-align: center;">TOTAL</th>
					<th style="text-align: center;">REMOVE</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="o" items="${order.items}">
					<tr style="text-align: center;">
						<td><img
							src="${pageContext.request.contextPath }/upload/${o.product.image}"
							style="width: 100px; height: 100px"></td>
						<td style="vertical-align: middle; width: 300px;">${o.product.name }</td>
						<td style="vertical-align: middle;">${o.price}</td>
						<td style="vertical-align: middle; width: 150px;"><div
								class="add-to-cart">
								<div class="input-number">
									<input type="number" value="${o.quantity}"
										style="text-align: center;"> <span class="qty-up">+</span>
									<span class="qty-down">-</span>
								</div>
							</div>
						<td style="vertical-align: middle;">${o.quantity*o.price}</td>
						<td style="vertical-align: middle; width: 100px"><a
							href="${pageContext.request.contextPath}/delete-item?id=${o.product.id}"><img
								src="${pageContext.request.contextPath}/template/admin/img/trash.png"></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="row" style="width: 400px; height:100px; background-color: #f6f6f6;margin-left: 755px; font-size: 17px;">
		<div class="col-5" style="display: flex; margin: 20px;">
			<ul style="display: block;">
				<li style="display: block; margin-bottom: 10px;">Total Item </li>
				<li style="display: block; margin-bottom: 10px;">Grand total </li>
			</ul>
			<ul style="display: block; margin-left: 150px; float: left;">
				<li style="display: block; margin-bottom: 10px;">1500000 </li>
				<li style="display: block; margin-bottom: 10px;" >1500000 </li>
			</ul>
		
		</div>
	</div>
	<div class="row justify-content-between" style=" margin-top: 30px; display: flex;">
			<div class="col">
				<button type="button" class="btn btn-primary">Back home</button>
			</div>
		<div class="col"  style="float: left; margin-left: 960px;">
				<button type="button" class="btn btn-primary"><a href="${pageContext.request.contextPath }/checkout">Check out</a></button>
			</div>
	</div>
	
</div>
</c:if>
<c:if test="${empty order }">
	<div class="container">
	<!-- row -->
	<div class="row">
		<h2>Bạn chưa lựa chọn món hàng nào cả!</h2>
	</div> 
	</div>
</c:if>
<script>
	var deleteItem = $('.deleteItem');
	deleteItem.each(function() {
		$(this).click(function() {
			var id = $(this).data('id');
			alert(id);
		})
	})
</script>
