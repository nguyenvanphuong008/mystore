package myproject.springmvc.utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import myproject.springmvc.entity.ItemEntity;
import myproject.springmvc.entity.OrderEntity;
import myproject.springmvc.entity.Product;

public class CartUtils {

	public static void getCart(HttpServletRequest request, Product product) {
		int quantity = 1;
		if (product != null) {
			quantity = Integer.parseInt(request.getParameter("quantity"));
		}
		if (SessionUtils.getIntance().getValue(request, "order") == null) {
			OrderEntity order = new OrderEntity();
			ItemEntity item = new ItemEntity();
			List<ItemEntity> items = new ArrayList<ItemEntity>();
			item.setProduct(product);
			item.setQuantity(quantity);
			item.setPrice(product.getPrice());
			items.add(item);
			order.setItems(items);
			SessionUtils.getIntance().setValue(request, "order", order);
		} else {
			OrderEntity order = (OrderEntity) SessionUtils.getIntance().getValue(request, "order");
			List<ItemEntity> items = order.getItems();
			boolean check = false;
			for (ItemEntity item : items) {
				if (item.getProduct().getId() == product.getId()) {
					item.setQuantity(item.getQuantity() + quantity);
					check = true;
				}
			}
			if (check == false) {
				ItemEntity newItem = new ItemEntity();
				newItem.setProduct(product);
				newItem.setQuantity(quantity);
				newItem.setPrice(product.getPrice());
				items.add(newItem);
			}
			SessionUtils.getIntance().setValue(request, "order", order);
		}
	}
	public static int totalItem(HttpServletRequest request) {
		int totalItem=0;
		OrderEntity order =(OrderEntity) SessionUtils.getIntance().getValue(request, "order");
		List<ItemEntity> items = order.getItems();
		for(ItemEntity item:items) {
			totalItem += item.getQuantity();
		}
		SessionUtils.getIntance().setValue(request, "totalItem", totalItem);
		return totalItem;
		
	}
	public static void deleteItem(HttpServletRequest request, int id) {
		OrderEntity order =(OrderEntity) SessionUtils.getIntance().getValue(request, "order");
		List<ItemEntity> items = order.getItems();
		for(int i=0; i<items.size();i++) {
			if(items.get(i).getProduct().getId()== id) {
				items.remove(i);
			}
		}
		SessionUtils.getIntance().setValue(request, "order", order);
	}

}
