package myproject.springmvc.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {
	
	private static SessionUtils sessionUtils;
	
	public static SessionUtils getIntance() {
		if(sessionUtils == null ) {
			sessionUtils =  new SessionUtils();
		}
			return sessionUtils;
		}
	
	public void setValue(HttpServletRequest request,String key, Object value) {
		HttpSession session = request.getSession();
		session.setAttribute(key, value);
	}
	
	public Object getValue(HttpServletRequest request,String key) {
		HttpSession session = request.getSession();
		return session.getAttribute(key);
	}
	public void removeValue(HttpServletRequest request,String key) {
		request.getSession().removeAttribute(key);
	}

}
