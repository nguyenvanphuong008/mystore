package myproject.springmvc.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.web.multipart.MultipartFile;

public class UploadFileUtils {
	public static String upload(String path, MultipartFile part) throws IOException {
		String fileName = part.getOriginalFilename().replaceAll("\\s", "_");
		File file = new File(path + fileName);
		if (file.exists()) {
			return file.getName();
		} else {
			try (InputStream is = part.getInputStream()) {
				try (OutputStream os = new FileOutputStream(file)) {
					int len = 0;
					byte[] bytes = new byte[1024];
					while ((len = is.read(bytes, 0, 1024)) > 0) {
						os.write(bytes, 0, len);
					}
				}
			}
			return fileName;
		}
	}
}
