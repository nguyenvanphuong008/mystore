package myproject.springmvc.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import myproject.springmvc.service.ICategoryService;

@Controller
@RequestMapping("/admin")
public class CategoryController {
	
	@Autowired
	ICategoryService categoryService;
	
	@RequestMapping("/list-category.html")
	public ModelAndView listCategory() {
		ModelAndView mav = new ModelAndView("list.category");
		mav.addObject("list", categoryService.findAll());
		return mav;
	}
	@RequestMapping("/edit-category.html")
	public ModelAndView editCategory(@RequestParam("id") int id) {
		ModelAndView mav = new ModelAndView("edit.category");
		mav.addObject("category", categoryService.findById(id));
		return mav;
	}
	
//	@RequestMapping(value = "/save-product.html", method = RequestMethod.POST) 
//	public String saveProduct(Product product) {
//		productService.update(product,part,path);
//		return "redirect:/list-product.html";
//	}
//	@RequestMapping("/add-product.html")
//	public ModelAndView addProduct(Product product) {
//		ModelAndView mav = new ModelAndView("add.product");
//		mav.addObject("listOfCategory", categoryService.findAll());
//		mav.addObject("listOfBrand", brandService.findAll());
//		return mav;
//	}
//	
//	@RequestMapping("/search-product.html")
//	public ModelAndView searchProduct(String keyWord) {
//		ModelAndView mav = new ModelAndView("search.product");
//		mav.addObject("list", productService.search(keyWord));
//		return mav;
//	}
}
