package myproject.springmvc.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	@RequestMapping("/admin")
	public ModelAndView homeAdmin() {
		ModelAndView mav = new ModelAndView("home.admin");
		return mav;
	}
}
