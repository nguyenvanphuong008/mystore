package myproject.springmvc.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import myproject.springmvc.entity.Product;
import myproject.springmvc.service.IBrandService;
import myproject.springmvc.service.ICategoryService;
import myproject.springmvc.service.IProductService;

@Controller
@RequestMapping("/admin")
public class ProductController {
	
	@Autowired
	IProductService productService;
	@Autowired
	IBrandService brandService;
	@Autowired
	ICategoryService categoryService;
	
	@RequestMapping("/list-product.html")
	public ModelAndView listProduct() {
		ModelAndView mav = new ModelAndView("list.product");
		mav.addObject("list", productService.findAll());
		return mav;
	}
	@RequestMapping("/edit-product.html")
	public ModelAndView editProduct(@RequestParam("id") int id) {
		ModelAndView mav = new ModelAndView("edit.product");
		mav.addObject("product", productService.findById(id));
//		for(Brand brand : brandService.findAll()) {
//			Product product = new Product();
//			product.setBrand(brand);
//			listOfBrandAndCategory.add(product);
//		}
//		for(Category category : categoryService.findAll()) {
//			Product product = new Product();
//			product.setCategory(category);
//			listOfBrandAndCategory.add(product);
//		}
//		Map<Integer, String> map = new HashMap<Integer, String>();
//		for (Product item : listOfBrandAndCategory) {
//			map.put(item.getCategory().getId(), item.getCategory().getName());
//		}
//		mav.addObject("map", map);
		mav.addObject("listOfCategory", categoryService.findAll());
		mav.addObject("listOfBrand", brandService.findAll());
		return mav;
	}
	
	@RequestMapping(value = "/save-product.html", method = RequestMethod.POST) 
	public String saveProduct(Product product,@RequestParam("file") MultipartFile part,HttpServletRequest request) {
		String path = request.getServletContext().getRealPath("/upload/");
		productService.update(product,part,path);
		return "redirect:/admin/list-product.html";
	}
	@RequestMapping("/add-product.html")
	public ModelAndView addProduct(Product product) {
		ModelAndView mav = new ModelAndView("add.product");
		mav.addObject("listOfCategory", categoryService.findAll());
		mav.addObject("listOfBrand", brandService.findAll());
		return mav;
	}
	
	@RequestMapping("/search-product.html")
	public ModelAndView searchProduct(String keyWord) {
		ModelAndView mav = new ModelAndView("search.product");
		mav.addObject("list", productService.search(keyWord));
		return mav;
	}
}
