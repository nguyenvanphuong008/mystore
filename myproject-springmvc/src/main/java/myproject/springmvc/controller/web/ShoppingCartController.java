package myproject.springmvc.controller.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import myproject.springmvc.entity.Product;
import myproject.springmvc.service.IItemService;
import myproject.springmvc.service.IOrderService;
import myproject.springmvc.service.IProductService;
import myproject.springmvc.utils.CartUtils;

@Controller
public class ShoppingCartController {
	
	@Autowired
	IProductService  productService;
	
	@Autowired
	IOrderService orderService;
	
	@Autowired
	IItemService itemService;
	
	@RequestMapping("/view-cart")
	public ModelAndView viewCart(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("view.cart");
		return mav;
	}
	@RequestMapping(value="/add-item", method=RequestMethod.POST)
	public void addItem(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		try(PrintWriter pw = response.getWriter()){
			int id = Integer.parseInt(request.getParameter("data"));
			Product product = productService.findById(id);
			CartUtils.getCart(request,  product);
			pw.write(String.valueOf(CartUtils.totalItem(request)));
		}
		}
	@RequestMapping("/delete-item")
	public ModelAndView deleteItem(@RequestParam("id") int id,HttpServletRequest request) {
		ModelAndView mav = new  ModelAndView("redirect:/view-cart");
		CartUtils.deleteItem(request, id);
		CartUtils.totalItem(request);
		return mav;
	}
	
	@RequestMapping("/checkout")
	public ModelAndView checkOut() {
		ModelAndView mav; 
		Authentication auth= SecurityContextHolder.getContext().getAuthentication();
		if(auth.getName().equals("anonymousUser")) {
			mav = new ModelAndView("redirect:/login");
		}else {
			mav = new ModelAndView("checkout");
		}
		return mav;
	}
		
	}

