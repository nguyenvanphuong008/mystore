package myproject.springmvc.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import myproject.springmvc.entity.Category;
import myproject.springmvc.entity.Product;
import myproject.springmvc.service.IProductService;

@Controller(value="homeControllerOfWeb")
public class HomeController {
	
	@Autowired
	IProductService productService;
	
	@RequestMapping("/trang-chu")
	public ModelAndView homeAdmin() {
		Product product = new Product();
		Category category = new Category();
		category.setName("Laptop");
		product.setCategory(category);
		ModelAndView mav = new ModelAndView("trang.chu");
		mav.addObject("listOfProduct", productService.findAllNewLaptop(product.getCategory().getName()));
		return mav;
	}
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public ModelAndView loginPage() {
		ModelAndView mav = new ModelAndView("login.admin");
		return mav;
	}
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return new ModelAndView("redirect:/trang-chu");
	}
	
	@RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
	public ModelAndView accessDenied() {
		return new ModelAndView("redirect:/login?accessDenied");
	}
	
//	public ModelAndView getNewLaptop() {
//		ModelAndView mav = new ModelAndView("get.new.laptop");
//		return mav;
//	}
}
