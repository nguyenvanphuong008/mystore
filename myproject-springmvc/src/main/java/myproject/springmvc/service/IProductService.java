package myproject.springmvc.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import myproject.springmvc.entity.Product;

public interface IProductService {

	public List<Product> findAll();
	public Product findById(Integer id);
	public void update(Product product, MultipartFile part, String path);
	public List<Product> findAllByCategoryNameOrBrandNameIgnoreCaseContaining(String keyWord1,String keyWord2);
	//List<Product> findByName(String q);
	public  List<Product> search(String name);
	public List<Product> findAllNewLaptop (String name);
}
