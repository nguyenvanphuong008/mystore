package myproject.springmvc.service;

import java.util.List;

import myproject.springmvc.entity.Brand;


public interface IBrandService {

	public List<Brand> findAll();
}
