package myproject.springmvc.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import myproject.springmvc.entity.Product;
import myproject.springmvc.repository.IProductRepository;
import myproject.springmvc.service.IProductService;
import myproject.springmvc.utils.UploadFileUtils;

@Service
public class ProductService implements IProductService {

	@Autowired
	IProductRepository productRepository;

	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	public Product findById(Integer id) {
		return productRepository.findById(id);
	}

	@Override
	@Transactional
	public void update(Product product, MultipartFile part, String path) {
		if (part.getOriginalFilename().isEmpty()) {
			productRepository.save(product);
		} else {
			try {
				product.setImage(UploadFileUtils.upload(path, part));
			} catch (IOException e) {
				e.printStackTrace();
			}
			productRepository.save(product);
		}
	}

	@Override
	public List<Product> findAllByCategoryNameOrBrandNameIgnoreCaseContaining(String name,String keyWord2) {
		return productRepository.findAllByCategoryNameOrBrandNameIgnoreCaseContaining(name,keyWord2);
	}

	@Override
	public List<Product> search(String name) {
		return productRepository.search(name);
	}

	@Override
	public List<Product> findAllNewLaptop(String name) {
		return productRepository.findByCategoryName(name);
	}

//	@Override
//	public List<Product> findByName(String q) {
//		return productRepository.findByName(q);
//	}


}
