package myproject.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myproject.springmvc.entity.ItemEntity;
import myproject.springmvc.repository.IItemReposity;
import myproject.springmvc.service.IItemService;

@Service
public class ItemService implements IItemService {
	
	@Autowired
	IItemReposity itemRepository;
	
	@Override
	public void save(ItemEntity item) {
		itemRepository.save(item);
	}

}
