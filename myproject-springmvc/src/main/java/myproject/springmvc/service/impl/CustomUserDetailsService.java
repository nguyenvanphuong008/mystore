package myproject.springmvc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import myproject.springmvc.constant.SystemConstant;
import myproject.springmvc.dto.MyUser;
import myproject.springmvc.entity.Role;
import myproject.springmvc.entity.UserEntity;
import myproject.springmvc.repository.IUserRepository;



@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private IUserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findOneByEmailAndStatus(username, SystemConstant.ACTIVE_STATUS);
		if (userEntity == null) {
			throw new UsernameNotFoundException("User not found");
		}
		//put thong tin vao khi dang nhap thanh cong  va duy tri thong tin trong spring security.
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role: userEntity.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getCode()));
		}
		MyUser myUser = new MyUser(userEntity.getEmail(), userEntity.getPassword(), true, true, true, true, authorities);
		
		myUser.setFullName(userEntity.getName());
		return myUser;
	}

}