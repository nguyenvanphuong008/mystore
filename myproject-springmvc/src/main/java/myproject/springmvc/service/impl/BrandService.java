package myproject.springmvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myproject.springmvc.entity.Brand;
import myproject.springmvc.repository.IBrandRepository;
import myproject.springmvc.service.IBrandService;

@Service
public class BrandService implements IBrandService {

	@Autowired
	IBrandRepository brandRepository;

	@Override
	public List<Brand> findAll() {

		return brandRepository.findAll();
	}

}
