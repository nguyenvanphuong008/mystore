package myproject.springmvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myproject.springmvc.entity.OrderEntity;
import myproject.springmvc.repository.IOrderReposity;
import myproject.springmvc.service.IOrderService;

@Service
public class OrderService implements IOrderService {
	
	@Autowired
	IOrderReposity orderReposity;

	@Override
	public void save(OrderEntity order) {
		orderReposity.save(order);
	}

}
