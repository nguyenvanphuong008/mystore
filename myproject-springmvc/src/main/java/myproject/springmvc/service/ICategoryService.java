package myproject.springmvc.service;

import java.util.List;

import myproject.springmvc.entity.Category;

public interface ICategoryService {

	public List<Category> findAll();

	public Category findById(int id);
}
