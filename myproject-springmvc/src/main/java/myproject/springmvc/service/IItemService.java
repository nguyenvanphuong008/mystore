package myproject.springmvc.service;

import myproject.springmvc.entity.ItemEntity;

public interface IItemService  {
	
	public void save(ItemEntity item);
}
