package myproject.springmvc.service;

import myproject.springmvc.entity.OrderEntity;

public interface IOrderService {
	
	public void save(OrderEntity order);
}
