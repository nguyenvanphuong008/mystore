package myproject.springmvc.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "Brand")
public class Brand extends BaseEntity{
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "image")
	private String image;
	
	@OneToMany(mappedBy="brand", fetch= FetchType.LAZY )
	List<Product>products = new ArrayList<Product>();
	
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
