package myproject.springmvc.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "order")
public class OrderEntity extends BaseEntity {

	private String email;
	private String phone;

	@ManyToOne
	@JoinColumn(name = "addressid")
	private Address address = new Address();

	@ManyToMany
	@JoinTable(name = "user_order", joinColumns = @JoinColumn(name = "orderid"), inverseJoinColumns = @JoinColumn(name = "roleid"))
	private List<UserEntity> users;

	@OneToMany(mappedBy = "order")
	private List<ItemEntity> items;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(List<UserEntity> users) {
		this.users = users;
	}

	public List<ItemEntity> getItems() {
		return items;
	}

	public void setItems(List<ItemEntity> items) {
		this.items = items;
	}

}
