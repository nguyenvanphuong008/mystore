package myproject.springmvc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import myproject.springmvc.entity.Brand;

public interface IBrandRepository extends JpaRepository<Brand, Integer> {
	
	public List<Brand> findAll();
}
