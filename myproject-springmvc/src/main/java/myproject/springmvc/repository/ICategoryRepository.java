package myproject.springmvc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import myproject.springmvc.entity.Category;

public interface ICategoryRepository extends JpaRepository<Category, Integer> {
	
	public List<Category> findAll();
}
