package myproject.springmvc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import myproject.springmvc.entity.Product;

public interface IProductRepository extends JpaRepository<Product, Integer> {
	
	public List<Product> findAll();
	public Product findById(Integer id);
//	@Query(" SELECT p FROM Product p WHERE p.name = ?1 OR p.price = ?2 OR p.categoryName ?3 ")
//	  List<Product> findByName(String name, String pirce, String categoryName);
	@Query(" SELECT p FROM Product p WHERE p.name LIKE '%' || :p || '%' OR  p.category.name LIKE  '%' ||:p || '%' "
			+" OR p.brand.name LIKE  '%' ||:p || '%' ")
	  List<Product> search(@Param("p") String name);
	public List<Product> findAllByCategoryNameOrBrandNameIgnoreCaseContaining(String keyWord1,String keyWord2);
	public List<Product> findByCategoryName(String name);
}
