package myproject.springmvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myproject.springmvc.entity.UserEntity;

public interface IUserRepository extends JpaRepository<UserEntity, Integer> {
	public UserEntity findOneByEmailAndStatus(String email, int status);
}
