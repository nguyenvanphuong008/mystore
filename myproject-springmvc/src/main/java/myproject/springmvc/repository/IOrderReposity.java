package myproject.springmvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myproject.springmvc.entity.OrderEntity;

public interface IOrderReposity extends JpaRepository<OrderEntity, Integer>  {

}
