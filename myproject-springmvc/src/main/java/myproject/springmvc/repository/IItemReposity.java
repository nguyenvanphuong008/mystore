package myproject.springmvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myproject.springmvc.entity.ItemEntity;

public interface IItemReposity extends JpaRepository<ItemEntity, Integer> {

}
