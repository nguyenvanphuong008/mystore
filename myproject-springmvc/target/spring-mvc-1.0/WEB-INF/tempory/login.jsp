<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="container">
	<div class="card card-login mx-auto mt-5">
		<div class="card-header">Login</div>
		<div class="card-body">
			<form action="/j_spring_security_check" method="post">
				<div class="form-group">
					<div class="form-label-group">
						<input name="j_username" type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required"autofocus="autofocus"> 
						<label for="inputEmail">Email address</label>
					</div>
				</div>
				<div class="form-group">
					<div class="form-label-group">
						<input name="j_password" type="password" id="inputPassword" class="form-control"placeholder="Password" required="required"> 
						<label for="inputPassword">Password</label>
					</div>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label> <input type="checkbox" value="remember-me">Remember Password</label>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-block" >Login</button>
			</form>
			<div class="text-center">
				<a class="d-block small mt-3" href="register.html">Register anAccount</a> <a class="d-block small" href="forgot-password.html">ForgotPassword?</a>
			</div>
		</div>
	</div>
</div>