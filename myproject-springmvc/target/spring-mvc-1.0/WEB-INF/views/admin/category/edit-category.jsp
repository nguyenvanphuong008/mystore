<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>

<form:form action="${pageContext.request.contextPath}/save-product.html"  method="post" modelAttribute="category"  >
	<h1>Edit category</h1>
        <hr>
	<div class="form-group">
		<label>Name</label>
		<form:hidden path="id"/>
		<form:input path="name" type="text" class="form-control" />
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form:form>
