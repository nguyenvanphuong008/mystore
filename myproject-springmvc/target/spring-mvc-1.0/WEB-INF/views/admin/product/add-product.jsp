<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>

<form:form action="${pageContext.request.contextPath}/admin/save-product.html"  enctype="multipart/form-data"  method="post" modelAttribute="product">
	<h1>Add new product</h1>
        <hr>
	<div class="form-group">
		<label>Name</label>
		<form:input path="name" type="text" class="form-control" />
	</div>
	<div class="form-group">
		<label>Quantity</label>
		<form:input path="quantity" type="text" class="form-control" />
	</div>
	<div class="form-group">
		<label>Price</label>
		<form:input path="price" type="text" class="form-control" />
	</div>
	<div class="form-group">
		<label>Price sale off</label>
		<form:input path="saleOff" type="text" class="form-control" />
	</div>
	<div class="form-group">
		<label>Category</label>
		<form:select path="category.id" class="form-control">
			<form:options items="${listOfCategory}" itemLabel="name"  itemValue="id" />
		</form:select>
	</div>
	<div class="form-group">
		<label>Brand</label>
		<form:select path="brand.id" class="form-control">
			<form:options items="${listOfBrand}" itemLabel="name"  itemValue="id" />
		</form:select>
	</div>
	<div class="form-group">
		<label for="exampleFormControlTextarea1">Desription</label>
		<form:textarea path="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></form:textarea>
	</div>
	<div class="form-group">
		<label for="exampleFormControlFile1">Choose image file</label> 
		<input name="file" type="file" class="form-control-file" id="exampleFormControlFile1" onchange ="getEditImageProduct(this)"/>
	</div>
	<img id="geteditimage" src="" class="rounded mx-auto d-block" alt="">
	
	<button type="submit" class="btn btn-primary">Submit</button>
</form:form>
<script>
   function getEditImageProduct(input) {
    			 if (input.files && input.files[0]) {
         	var reader = new FileReader();

         reader.onload = function (e) {
             $('#geteditimage')
                 .attr('src', e.target.result)
                 .width(400)
                 .height(400)
                 }

         reader.readAsDataURL(input.files[0]);
       }
   }
</script>
