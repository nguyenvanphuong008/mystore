	<div id="hot-deal" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="hot-deal" id="countdown">
							<ul class="hot-deal-countdown">
								<li>
									<div>
										<h3 id = "day">02</h3>
										<span>Days</span>
									</div>
								</li>
								<li>
									<div>
										<h3 id = "hour">10</h3>
										<span>Hours</span>
									</div>
								</li>
								<li>
									<div>
										<h3 id = "minute">34</h3>
										<span>Mins</span>
									</div>
								</li>
								<li>
									<div>
										<h3 id = "second">60</h3>
										<span>Secs</span>
									</div>
								</li>
							</ul>
							<h2 class="text-uppercase">hot deal this week</h2>
							<p>New Collection Up to 50% OFF</p>
							<a class="primary-btn cta-btn" href="#">Shop now</a>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
	<script>
		// set the date we're counting down to
	var target_date = new Date('November, 14, 2019').getTime();
	 
	// variables for time units
	var days, hours, minutes, seconds;
	 
	// get tag element
	var countdown = document.getElementById('countdown');
	 
	// update the tag with id "countdown" every 1 second
	setInterval(function () {
	 
	    // find the amount of "seconds" between now and target
	    var current_date = new Date().getTime();
	    //var seconds_left = (current_date - target_date) / 1000;
	 	var seconds_left = (target_date - current_date) / 1000;
	    // do some time calculations
	    days = parseInt(seconds_left / 86400);
	    seconds_left = seconds_left % 86400;
	     
	    hours = parseInt(seconds_left / 3600);
	    seconds_left = seconds_left % 3600;
	     
	    minutes = parseInt(seconds_left / 60);
	    seconds = parseInt(seconds_left % 60);
	     
	    // format countdown string + set tag value
		day.innerHTML = days;
		hour.innerHTML = hours;
		minute.innerHTML = minutes;
		second.innerHTML = seconds
	}, 1000);
	</script>
