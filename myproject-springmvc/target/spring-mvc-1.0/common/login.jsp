<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- <meta name="description" content="">
<meta name="author" content=""> -->

<title><tiles:getAsString name="title"/></title>

<!-- Custom fonts for this template-->
<link
	href="${pageContext.request.contextPath}/template/admin/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template-->
<link
	href="${pageContext.request.contextPath}/template/admin/css/sb-admin.css"
	rel="stylesheet">

</head>
<body>
	<tiles:insertAttribute name="content"/>
	<!-- Bootstrap core JavaScript-->
	<script
		src="${pageContext.request.contextPath}/template/admin/vendor/jquery/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/template/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script
		src="${pageContext.request.contextPath}/template/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
